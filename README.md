# 810fr Cloture S01

Jupyter notebook associé aux analyses de l'article de clôture de la saison 01 de 810.fr : analyse l'engagement produit par les vidéos, par courant, sur YouTube.

Scatterplot ; regression linéaire ; graph de densité ; sélection par engagement seuil